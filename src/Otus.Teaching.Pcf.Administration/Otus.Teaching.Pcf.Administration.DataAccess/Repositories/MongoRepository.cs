﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using MongoDB.Driver;
using Microsoft.Extensions.Options;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _administrationDC;

        //--------------------------------------------------------------------------------------------------------
        public MongoRepository(IOptions<MongoDbSettings> mongoDb)
        {
            _administrationDC = new MongoClient(mongoDb.Value.ConnectionString)
                .GetDatabase(mongoDb.Value.DatabaseName)
                .GetCollection<T>(typeof(T).Name.ToString());
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            List<T> outV = null;
            try
            {
                outV = await _administrationDC.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return outV;
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _administrationDC.Find(rec => rec.Id == id).FirstOrDefaultAsync();

            return entity;
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _administrationDC.Find(rec => ids.Contains(rec.Id)).ToListAsync();
            return entities;
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _administrationDC.Find(predicate).FirstOrDefaultAsync();
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _administrationDC.Find(predicate).ToListAsync();
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task AddAsync(T entity)
        {
            await _administrationDC.InsertOneAsync(entity);
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task UpdateAsync(T entity)
        {
            await _administrationDC.ReplaceOneAsync(rec => rec.Id == entity.Id, entity);
        }

        //--------------------------------------------------------------------------------------------------------
        public async Task DeleteAsync(T entity)
        {
            await _administrationDC.DeleteOneAsync(rec => rec.Id == entity.Id);
        }

        //--------------------------------------------------------------------------------------------------------
        public void AddRange(List<T> entities)
        {
            _administrationDC.InsertMany(entities);
        }

        //--------------------------------------------------------------------------------------------------------
        public void EmtyData()
        {
            _administrationDC.DeleteMany(_ => true);
        }
    }
}
