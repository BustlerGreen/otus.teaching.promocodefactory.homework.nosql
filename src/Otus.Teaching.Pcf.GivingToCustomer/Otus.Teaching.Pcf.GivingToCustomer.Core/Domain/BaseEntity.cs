﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class BaseEntity
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.String)] 
        public Guid Id { get; set; }
    }
}